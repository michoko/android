# Participants #
- Damien DELOCHE
- Stéphane GUARDO
- Vincent LAFOSSE

# Description du dépôt #
Dépôt Git destiné à héberger, versionner le code du client Android du projet IoT.
Il recense également les objectifs voulus, les difficultés rencontrées et les solutions prises.